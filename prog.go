package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"
)

func loadWords(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	words := make([]string, 0)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		ts := scanner.Text()
		if len(ts) > 4 && len(ts) < 8 {
			words = append(words, scanner.Text())
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return words
}

func main() {
	var wordCount = flag.Int("count", 3, "count of words")
	flag.Parse()

	//fmt.Println("ip has this value", *wordCount)
	rand.Seed(time.Now().UTC().UnixNano())

	conns := loadWords("/usr/share/dict/connectives")
	//words := loadWords("/usr/share/dict/words")
	props := loadWords("/usr/share/dict/propernames")
	for i := 0; i < *wordCount; i++ {
		fmt.Printf("%s ", props[rand.Intn(len(props))])
		fmt.Printf("%s ", conns[rand.Intn(len(conns))])
	}
	fmt.Println()
}
